#!/bin/bash

###############################################################################
# 
# SCRIPT NAME: arch_install.sh
#
# DESCRIPTION: This script performs an arch base install on both UEFI and BIOS
# AUTHOR: Jonas Ingemarsson
# VERSION: 1.0
# DATE: April 17, 2023
# USAGE: ./arch_install.sh
#
# NOTES: This script requires a bootable Arch Linux ISO.
#
# DISCLAIMER: Use this script at your own risk. The author assumes no liability 
# for any damage or loss caused by the use or misuse of this script.
#
###############################################################################

# Packages to be installed by pacstrap command
PACSTRAP="base base-devel linux linux-firmware vim networkmanager"

# Additional software to be installed
SOFTWARE="grub linux-headers mtools dosfstools openssh"

# Function to partition the disk for UEFI systems
# This function partitions the disk using a GPT partition table.
# It creates an EFI system partition (ESP) and a root partition.
# The ESP is formatted as FAT32 and mounted to /mnt/boot/EFI.
# The root partition is formatted as ext4 and mounted to /mnt.
# Then the function calls the base_install function to continue with the installation.
uefi_partitioning(){
    echo "mklabel gpt
mkpart ESP fat32 1MiB 551MiB
set 1 esp on
mkpart primary ext4 551MiB 100%
print
" | parted $DEVICE
    mkfs.fat -F32 ${DEVICE}1
    mkfs.ext4 ${DEVICE}2
    mount ${DEVICE}2 /mnt
    mkdir -p /mnt/boot/EFI
    mount ${DEVICE}1 /mnt/boot/EFI
    base_install
}

# Function to partition the disk for BIOS systems
# This function partitions the disk using a GPT partition table.
# It creates a root partition and formats it as ext4.
# The root partition is then mounted to /mnt.
# Then the function calls the base_install function to continue with the installation.
bios_partitioning(){
    echo "mklabel gpt
mkpart primary ext4 1MiB 100%
print
" | parted $DEVICE
    mkfs.ext4 ${DEVICE}1
    mount ${DEVICE}1 /mnt
    base_install
}

# Function to install the base system and other required software
# This function installs the base system and other required software using the pacstrap command.
# It generates an fstab file based on the current disk configuration using genfstab.
# Then it calls the chroot_process function to configure the system inside the chroot environment.
base_install(){
    pacstrap /mnt $PACSTRAP
    genfstab -U /mnt >> /mnt/etc/fstab
    chroot_process
}

# This function is responsible for configuring the installed system after the base installation has completed
# It sets up the system timezone, hostname, root password, locale, sudoers file, NetworkManager, and installs additional software
# It also installs and configures the GRUB bootloader for both UEFI and BIOS systems
chroot_process(){
    arch-chroot /mnt /bin/bash -c '
sed -i s"/#Color/Color/" /etc/pacman.conf
sed -i s"/#ParallelDownloads/ParallelDownloads/" /etc/pacman.conf
ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime
hwclock --systohc
sed -i s"/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen
locale-gen
echo '"$HOSTNAME"' | tee /etc/hostname
echo -e "\n127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t'"$HOSTNAME"'.localdomain\t\t'"$HOSTNAME"'" >> /etc/hosts
echo "root:'"$ROOTPASS"'" | chpasswd
sed -i s"/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/" /etc/sudoers
systemctl enable NetworkManager
echo "LANG=en_US.UTF-8" > /etc/locale.conf
pacman -S '"$SOFTWARE"' --noconfirm
if test -d "/sys/firmware/efi"; then
    grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=grub_uefi --recheck
else
    grub-install '"$DEVICE"'
fi
grub-mkconfig -o /boot/grub/grub.cfg
exit'
    echo KEYMAP=sv-latin1 | tee /mnt/etc/vconsole.conf
    umount -R /mnt
}

### START HERE

# Define colors
GREEN='\033[0;32m'
YELLOW='\033[93m'
RED='\033[31m'
NC='\033[0m'

# Prompt user for root password
read -s -p "$(echo -e "${YELLOW}Please set password for root:${NC} ")" ROOTPASS
echo ""

# Prompt user for hostname
read -p "$(echo -e "${YELLOW}Please set hostname:${NC} ")" HOSTNAME
echo ""

# Show available disks and prompt user for installation device
lsblk
echo ""
echo -e "${GREEN}Default installation device is /dev/sda.${NC}"
read -p "$(echo -e "Press ${GREEN}ENTER${NC} or type other device, ${YELLOW}example /dev/sdb${NC}: ")" DEVICE
DEVICE="${DEVICE:=/dev/sda}"
echo -e "You have chosen ${RED}${DEVICE}${NC}"

# Prompt user to confirm device selection
read -p "Do you want to continue? (Y/N) " answer
if [[ $answer =~ ^[Yy]$ ]]; then
    echo "Continuing..."
elif [[ $answer =~ ^[Nn]$ ]]; then
    echo "Exiting."
    exit 0
else
    echo "Invalid input. Please enter Y or N."
    exit 0
fi

# Enable color and parallel downloads in pacman.conf
sed -i 's/#\(Color\|ParallelDownloads\)/\1/' /etc/pacman.conf

# Partition disk based on whether system is using UEFI or BIOS
if test -d "/sys/firmware/efi"; then
    SOFTWARE+=" efibootmgr"
    uefi_partitioning
else
    bios_partitioning
fi

# Inform user of successful installation and prompt for reboot
echo -e "${GREEN}##### INSTALLATION COMPLETE! #####"
echo -e "${YELLOW}Please remove installation media and reboot...${NC}"